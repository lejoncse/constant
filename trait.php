<?php

trait sharable{
    public function share(){
        return "the name of trait is :" .__TRAIT__;
    }
}

class Post{
    use sharable;
}

$post= new Post;
echo $post->share();